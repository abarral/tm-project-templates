package com.topmanage.jiraplugins.projecttemplates.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
  
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.bc.user.search.UserSearchParams;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.*;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import com.topmanage.jiraplugins.projecttemplates.ProjectCloner;
import com.topmanage.jiraplugins.projecttemplates.ProjectCreationException;


@SuppressWarnings("serial")
public class JiraTemplates extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(JiraTemplates.class);

    
    private IssueService issueService;
    private ProjectService projectService;
    private SearchService searchService;
    private com.atlassian.sal.api.user.UserManager userManager;
    private TemplateRenderer templateRenderer;
    private ProjectManager projectManager;
    private ProjectCloner projectCloner;
    private com.atlassian.jira.user.util.UserManager jiraUserManager;
    private static final String LIST_BROWSER_TEMPLATE = "/templates/list.vm";
    private static final String NEW_BROWSER_TEMPLATE = "/templates/new.vm";
    private static final String EDIT_BROWSER_TEMPLATE = "/templates/edit.vm";
     
    public static final String TEMPLATE_CATEGORY_NAME = "Templates";


    private UserSearchService userSearchService;


    private JiraAuthenticationContext jiraAuthenticationContext;
    
    public JiraTemplates(IssueService issueService, ProjectService projectService, 
                     SearchService searchService, com.atlassian.sal.api.user.UserManager userManager,
                     com.atlassian.jira.user.util.UserManager jiraUserManager,
                     TemplateRenderer templateRenderer, ProjectManager projectManager,
                     ProjectCloner projectCloner, UserSearchService userSearchService, 
                     JiraAuthenticationContext jiraAuthenticationContext) {
        this.issueService = issueService;
        this.projectService = projectService;
        this.searchService = searchService;
        this.userManager = userManager;
        this.templateRenderer = templateRenderer;
        this.jiraUserManager = jiraUserManager;
        this.projectManager = projectManager;
        this.projectCloner = projectCloner;
        this.userSearchService = userSearchService;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        
    }
    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
                           throws ServletException, IOException {
     
        	ProjectCategory templateProjectCategory = projectManager.getProjectCategoryObjectByNameIgnoreCase(TEMPLATE_CATEGORY_NAME);
        	
        	Collection<Project> projects = Collections.emptyList();
        	if (templateProjectCategory != null){
        		projects = projectManager.getProjectsFromProjectCategory(templateProjectCategory);	
        	}
        	
        	//Get a list of available categories except "Templates" category
        	Collection<ProjectCategory> projectCategories = projectManager.getAllProjectCategories();
        	
        	Map<Long, ProjectCategory> mappedProjectCategories = new HashMap<Long, ProjectCategory>();
        	
        	
        	for (ProjectCategory projectCategory : projectCategories){	
        		mappedProjectCategories.put(projectCategory.getId(), projectCategory);
        	}
        	
        	if (templateProjectCategory != null)
        		mappedProjectCategories.remove(templateProjectCategory.getId());
        	
        	//Get a list of all users
        	Collection<ApplicationUser> users = userSearchService.findUsers("", UserSearchParams.ACTIVE_USERS_ALLOW_EMPTY_QUERY);
        	
        	String baseUrl = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
        	
        	Map<String, Object> context = Maps.newHashMap();
            context.put("projects", projects);
            context.put("projectCategories", mappedProjectCategories);
            context.put("users", users);
            context.put("baseUrl", baseUrl);
            resp.setContentType("text/html;charset=utf-8");
            
            templateRenderer.render("/templates/jiraTemplates.vm", context, resp.getWriter());
        }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res)
    		throws ServletException, IOException {
    	
    	Map<String, String[]> postValues = req.getParameterMap();
    	
    	String theTemplate = postValues.get("projectTemplate")[0];
    	Long newProjectCategory = Long.parseLong(postValues.get("projectCategory")[0]);
    	String newProjectKey = postValues.get("projectKey")[0];
    	String newProjectName = postValues.get("projectName")[0];
    	String newProjectLead = postValues.get("projectLeader")[0];
    	
    	ProjectCategory newProjectCategoryObj = projectManager.getProjectCategoryObject(newProjectCategory);
    	Map<String, Object> context = Maps.newHashMap();
    	
    	try{
    		Project newProject = projectCloner.PerformClone
    	    		(theTemplate, newProjectKey, newProjectName, newProjectCategoryObj, true, true, true, jiraAuthenticationContext.getLoggedInUser(),newProjectLead);

        	
        	context.put("reply", newProject);
        	
        	res.setContentType("text/html;charset=utf-8");
        	templateRenderer.render("/templates/performClone.vm", context, res.getWriter());
        	
    	}catch (ProjectCreationException e){
    		context.put("errors", e.errors);
        	
        	res.setContentType("text/html;charset=utf-8");
        	templateRenderer.render("/templates/performClone.vm", context, res.getWriter());
    	}
    	
    	  	
    	
    	
    	
    	
    }
    
}